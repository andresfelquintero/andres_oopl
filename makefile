.DEFAULT_GOAL := all

all:

clean:
	cd examples/c++; make clean

config:
	git config -l

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/oopl-fnal.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Hello.c++"                  \
    --include "Assertions.c++"             \
    --include "UnitTests1.c++"             \
    --include "UnitTests2.c++"             \
    --include "UnitTests3.c++"             \
    --include "Coverage1.c++"              \
    --include "Coverage2.c++"              \
    --include "Coverage3.c++"              \
    --include "IsPrime.c++"                \
    --include "IsPrimeT.c++"               \
    --include "Variables.c++"              \
    --include "Arguments.c++"              \
    --include "Returns.c++"                \
    --include "Incr.c++"                   \
    --include "Exceptions.c++"             \
    --exclude "*"                          \
    ../../examples/c++/ examples/c++/
